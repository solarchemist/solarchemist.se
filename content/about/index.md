---
title: "About"
date: "2023-02-20T18:00:00+01:00"
---


Welcome to my corner of the web!
My name is [Taha Ahmed](/contact) and I'm the author of this site.

I like the idea of giving back to the community by building on/with free software
and free knowledge in general, and as such I take every opportunity to promote
open science, open data, and open source.[^1]
All of which is made possible by the open web.[^3]

I maintain many [self-hosted](https://blog.ssdnodes.com/blog/what-is-self-hosting)
services, here are some I would like to mention:

+ my [linkblog](https://links.solarchemist.se/) where I share interesting tidbits
  I've encountered while browsing the web (sometimes with commentary).
  You can subscribe to it using [its RSS feed](https://links.solarchemist.se/feed/atom).
+ my [self-hosted Gitea instance](https://git.solarchemist.se).
+ my [public Shiny server](https://shiny.solarchemist.se).
  If you work with UV-Vis spectroscopy of semiconductors, check out
  my [Tauc fit evaluator](https://shiny.solarchemist.se/uvvistauc-reactive/).
  I have written a [how-to install Shiny](/2014/07/17/installing-shiny/), but
  that post should really be updated...
+ a few RStudio Server instances (primarily for my own use).
  I have an old [write-up on that setup](/2019/01/20/rstudio-server-multiple-instances-kvm/)
  (they are now run in LXC containers managed entirely via Ansible playbook, so the config
  described in that post is woefully out-dated...).
+ a [JupyterHub](https://jupyter.org/) instance for my own use, which I've setup with Python,
  Julia and R kernels.

The site header contains links to some of my web profiles
(I try to use only libre services, but it's a work in progress :-), and here
are some more of them that did not make it into the header:

+ [figshare.com](https://figshare.com/authors/Taha_Ahmed/279037)
+ [impactstory.org](https://impactstory.org/u/solarchemist)
+ [reddit.com](https://www.reddit.com/u/solarchemist)


The source code of this blog (including that of the blog posts themselves) is
[hosted on codeberg.org](https://codeberg.org/taha/solarchemist.se).

I have chosen not to use a regular comment system for this blog.
Instead I'd like to encourage you, dear reader, to use [hypothes.is](https://hypothes.is)
which effectively provides an *annotation layer for the web*.
You'll see a sidebar to the right --- open it to annotate!
Please note that I have **no control** over the content of these annotations,
it's up to you and other web users whether to enable this annotation layer or not.
It's possible to receive all annotations made on this blog by subscribing to
[this customised hypothes.is stream (Atom feed)](https://hypothes.is/stream.atom?wildcard_uri=https://solarchemist.se/*).

You may of course also send me comments via email or any of the channels listed above.

[^2]: What's RSS, you ask?
RSS is a standard for the syndication of content.
It's a little like XML or HTML --- it's not centralised, and by itself,
it ain't much, but in combination with the right software, it can put a lot of
power in the hands of users.
[Laura Kalbag has a good explainer on how to read RSS](https://laurakalbag.com/how-to-read-rss-in-2020/),
as [does p1k3](https://p1k3.com/2020/5/8/).
You use RSS by subscribing to RSS feeds (offered by the website you want to follow)
using your feed reader (also called [news aggregator](http://scripting.com/davenet/2002/10/08/whatIsANewsAggregator.html)).
The feed reader is just a piece of software (and you should get your own),
with [many to choose from](https://en.wikipedia.org/wiki/Comparison_of_feed_aggregators)).
Personally, I self-host [TinyTinyRSS](https://tt-rss.org/) on a server in
my closet --- it's been rock-solid for years :-)

[^1]: See, for example: [Alan Jacobs, Tending the Digital Commons: A Small Ethics toward the Future. The Hedgehog Review: Vol. 20 No. 1 (Spring 2018).](https://wallabag.chepec.se/share/61108bd298b9f1.94166093)
<!--
    original URL is no longer working
    http://www.iasc-culture.org/THR/THR_article_2018_Spring_Jacobs.php
    the full text can be found in some other places on the web
    unfortunately the hedgehog review itself has moved the essay behind a paywall
    https://hedgehogreview.com/issues/the-human-and-the-digital/articles/tending-the-digital-commons
-->

[^3]: Interested in the subject? A couple of excellent essays, both by Parimal Satyal:
[Rediscovering the small web](https://neustadt.fr/essays/the-small-web/), and
[Against an increasingly user-hostile web](https://neustadt.fr/essays/against-a-user-hostile-web/).
