---
title: "Contact"
date: "2023-02-20T18:00:00+01:00"
---


# There is always email...

Email me at `solarchemist [at] chepec [dot] se`.

# Mastodon, Matrix, and other ways

+ <code><a rel="me" href="https://scholar.social/@solarchemist">@solarchemist@scholar.social</a></code>
+ <code><a rel="me" href="https://social.sunet.se/@solarchemist">@solarchemist@social.sunet.se</a></code>
+ [`@solarchemist:matrix.org`](https://matrix.to/#/@solarchemist:matrix.org)
+ If you must, there is also [`solarchemist@linkedin.com`](https://se.linkedin.com/in/solarchemist)

Or feel free to give me a holler via any of the services displayed in the site header.

To follow this blog, please [subscribe to the RSS feed](/index.xml).
