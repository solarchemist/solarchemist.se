---
title: "Credits"
date: "2021-08-09T18:00:00+01:00"
---


<br />

I could not have created this website without the prior work of many others,
who generously chose to publish their work on the web and to license it freely.

These are some of the major building blocks of this website:

+ [Hugo](https://gohugo.io/), a very fast static site generator.
+ My own [fork](https://github.com/solarchemist/ghostwriter/) of
  the Hugo theme [Ghostwriter](https://github.com/jbub/ghostwriter).
+ The R packages [blogdown](https://github.com/rstudio/blogdown) 
  and [knitr](https://yihui.name/knitr/).
+ The extremely versatile document conversion tool [pandoc](https://pandoc.org/).
+ [MathJax](https://www.mathjax.org/), a client-side JavaScript implementation of LaTeX.
+ Icons from [Fork Awesome](https://forkawesome.github.io/Fork-Awesome/) 
  and [Academicons](https://jpswalsh.github.io/academicons/).

The posts on this blog are written in [R Markdown](https://bookdown.org/yihui/rmarkdown/basics.html),
with [source code for each post available](https://codeberg.org/taha/solarchemist.se/src/branch/master/content/post).

Thanks to pandoc and MathJax, the R Markdown format allows us to use familiar LaTeX notation
and a bunch of vital academic-blogging features such as citations, quotes, and footnotes in
our blog posts.
