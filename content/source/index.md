---
title: "Sources and license"
date: "2022-06-13T18:00:00+01:00"
---

Unless otherwise noted,
text and figures on this site are licensed under
[Creative Commons BY-SA v4.0](https://creativecommons.org/licenses/by-sa/4.0).

[![Creative Commons BY-SA](assets/ccbysa.png)](https://creativecommons.org/licenses/by-sa/4.0)

[![GNU GPLv3](assets/gplv3.png)](https://www.gnu.org/licenses/gpl-3.0.en.html)

Code is licensed under [GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html),
with the source code of this site available at
[Codeberg](https://codeberg.org/solarchemist/solarchemist.se).
Each blog post has its
[own git repository](https://codeberg.org/solarchemist/solarchemist.se/src/branch/main/content/post)
(using so-called [*git submodules*](https://chrisjean.com/git-submodules-adding-using-removing-and-updating/))
inside that main repository.

<!-- https://get-it-on.codeberg.org -->
[![Blog source code on Codeberg](assets/codeberg-badge-bd0000-288.png)](https://codeberg.org/solarchemist/solarchemist.se)

Feel free to comment on this blog using [hypothes.is](https://hypothes.is/)
(available from the right sidebar).
If you have a more technical comment, please feel free to open an issue in either
the [main blog repo](https://codeberg.org/solarchemist/solarchemist.se) or in
[a specific blog post's repo](https://codeberg.org/solarchemist/solarchemist.se/src/branch/main/content/post).

[![hypothes.is - an annotating layer for the web](assets/hypothesis-288.png)](https://hypothes.is)
