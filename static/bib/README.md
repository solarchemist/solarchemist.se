# Remarks on CSL styles

## revista-materia.csl

Pro: The first style that does not create ugly newlines in the
  bibliography. Plus, handles page numbers in citations correctly!

Cons: Uses all-caps for author names. Could use some more vertical space
  between entries in the bibliography.

I created my own version of this CSL `revista-materia-solarchemist.csl`,
where I managed to change all-uppercase to normal capitalisation, but I
was not able to add more vertical space between entries (neither `entry-spacing`
nor `line-spacing` had any apparent effect).


## materials-chemistry-and-physics.csl

Pro: Citations in numeric style with square brackets.

Cons: List of references with ugly newline after each number.



## royal-society-of-chemistry-with-titles.csl

Cons: citations as superscript numbers (easily confused with
  footnotes). Newline after each number in list of references.



## Links

+ https://www.zotero.org/styles
+ https://editor.citationstyles.org/searchByName
+ https://editor.citationstyles.org/searchByExample
+ https://docs.citationstyles.org/en/stable/specification.html#bibliography-specific-options
+ https://citationstyles.org
